<?php

namespace AllForKids\EspaceSanteBundle\Controller;

use AllForKids\MainBundle\Entity\Vaccin;
use AllForKids\MainBundle\Form\VaccinType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('@EspaceSante/Layout/espacesantehome..html.twig');
    }

    public function AddVaccinAction(Request $request)
    {


        $em = $this->getDoctrine()->getManager();
        $vaccin = new Vaccin();
        $form = $this->createForm(VaccinType::class, $vaccin);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $data = $form->getData();
            $nom = $data->getNom();
            $age = $data->getAge();
            $matricule = $data->getMatricule();
            $desc = $data->getDescription();

            $em->persist($vaccin);
            $em->flush();

            return $this->redirectToRoute('espace_sante_vaccins');

        }



        return $this->render('@EspaceSante/Layout/addvaccin..html.twig',array('f' => $form->createView()));
    }




    public function listedesvaccinsAction()
    {


        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('AllForKidsMainBundle:Vaccin')->findAll();

        return $this->render('@EspaceSante/Layout/espacesantehome..html.twig',array('item'=>$item));
    }

    public function deleteVaccinAction($id)
    {



        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('AllForKidsMainBundle:Vaccin')->find($id);
        $em->remove($item);
        $em->flush();

        $item = $em->getRepository('AllForKidsMainBundle:Vaccin')->findAll();


        return $this->render('@EspaceSante/Layout/Listedesvaccins..html.twig',array('item'=>$item));
    }

    public function editVaccinAction(Request $request,$id)
    {



        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('AllForKidsMainBundle:Vaccin')->find($id);
        $form = $this->createForm(VaccinType::class, $item);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $em->persist($item);
            $em->flush();

            return $this->redirectToRoute('espace_sante_vaccins');

        }



        return $this->render('@EspaceSante/Layout/Modfiervaccin..html.twig',array('form'=>$form->createView()));
    }

    public function listedespediatresAction()
    {




        return $this->render('@EspaceSante/Layout/listedespediatres..html.twig');
    }

    function  caldendarAction(){
        return $this->render('@EspaceSante/Layout/calendar.html.twig');
    }

}
