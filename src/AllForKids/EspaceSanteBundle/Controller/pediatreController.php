<?php

namespace AllForKids\EspaceSanteBundle\Controller;

use AllForKids\MainBundle\Entity\Rdv;
use AllForKids\MainBundle\Entity\User;
use AllForKids\MainBundle\Form\RdvType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class pediatreController extends Controller
{
    public function listedespediatresAction()
    {
        $em = $this->getDoctrine()->getEntityManager();


        $qb = $em->getRepository('AllForKidsMainBundle:User')->createQueryBuilder('u');
        $qb->select('u')
            ->where('u.roles LIKE :roles')
            ->setParameter('roles', '%"ROLE_PEDIATRE"%');
        $item =  $qb->getQuery()->getResult();
        return $this->render('@EspaceSante/Layout/listedespediatres..html.twig',array('item'=>$item,'cc'=>count($item)));
    }


    public function showPediatreAction($id,Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneById($id);


        $res = $this->lookup($user->getAdresse());



        $rdv = new Rdv();
        $form = $this->createForm(RdvType::class, $rdv);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $data = $form->getData();
            $date = $data->getDate() ;

            //get month & days

            $day = date("d",strtotime($date->format('Y-m-d H:i:s')));
            $month = date("m",strtotime($date->format('Y-m-d H:i:s')));



            if ((int)date("m") < (int)$month){

                $date = $data->getDate() ;
                $nom = $data->getNomEnfant();
                $vaccin = $data->getVaccin();


                $rdv->setPediatre($user->getUsername());


                $em->persist($rdv);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'Succe',
                    'Ajout effectue!'
                );


                return $this->render('@EspaceSante/Layout/showPediatre.html.twig',array('res'=>$res,'user'=>$user,'f'=>$form->createView()));
            }

            else if ((int)date("d") < (int)$day){
                $date = $data->getDate() ;
                $nom = $data->getNomEnfant();
                $vaccin = $data->getVaccin();


                $rdv->setPediatre($user->getUsername());


                $em->persist($rdv);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                    'Succe',
                    'Ajout effectue!'
                );


                return $this->render('@EspaceSante/Layout/showPediatre.html.twig',array('res'=>$res,'user'=>$user,'f'=>$form->createView()));
            }
            else{
                $this->get('session')->getFlashBag()->add(
                    'Attention',
                    'Verifier le jour de RDV!'
                );
            }
        }








        return $this->render('@EspaceSante/Layout/showPediatre.html.twig',array('res'=>$res,'user'=>$user,'f'=>$form->createView()));
    }

    function lookup($string){

        $string = str_replace (" ", "+", urlencode($string));
        $details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$string."&sensor=false";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $details_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = json_decode(curl_exec($ch), true);


        // If Status Code is ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED or INVALID_REQUEST
        if ($response['status'] != 'OK') {
            return null;
        }

        $geometry = $response['results'][0]['geometry'];

        $longitude = $geometry['location']['lng'];
        $latitude = $geometry['location']['lat'];




        $array = array(
            'latitude' => $geometry['location']['lat'],
            'longitude' => $geometry['location']['lng'],
            'location_type' => $geometry['location_type'],
        );



        return $array;

    }

}
