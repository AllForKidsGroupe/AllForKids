<?php

namespace AllForKids\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\ColumnChart;
use Symfony\Component\HttpFoundation\Request;

class AccueilController extends Controller
{
    public function AccueilAction()
    {
        return $this->render('@AllForKidsMain/Accueil/accueil.html.twig', array(

        ));
    }
    public function chartAction(Request $request)
    {
        $enfant = $request->query->get('enfant');
        $theme = $request->query->get('theme');
        $params = $request->query->all();
        $themes = array("math"=>'Mathématique',"science"=>'Science',"culture"=>'Culture Générale',"langue"=>'Langue');
        $user = $this->getUser();
        $result = array();
        if(!empty($user)){
            $em = $this->getDoctrine()->getManager();
            $enfants = $em->getRepository("AllForKidsMainBundle:Enfant")->findBy(array('idParent'=>$user));
            if(empty($theme)){
                foreach ($themes as $key => $label) {
                    $sql = "select sum(score) from score where id_enfant = ? and id_quiz_id in (select id from quiz where theme = ? )";
                    $score = $em->getConnection()->fetchColumn($sql, array($enfant,$label), 0);
                    $result[$key] = $score;
                }

            }else{
                $key = array_search($theme,$themes);
                $sql = "select sum(score) from score where id_enfant = ? and id_quiz_id in (select id from quiz where theme = ? )";
                $result[$key] = $em->getConnection()->fetchColumn($sql, array($enfant,$theme), 0);
            }
            $barTheme[] = ['v' => [], 'f' => ''];
            foreach ($themes as $key => $value) {
                if(!array_key_exists($key,$result)){
                    $barTheme[] = 0;
                    $barTheme[] = '0';
                }else{
                    if($result[$key] == null)
                        $result[$key] = '0';
                    $barTheme[] = $result[$key];
                    $barTheme[] = $result[$key];
                }
            }
            //var_dump($barTheme);exit();
            $col = new ColumnChart();
            $col->getData()->setArrayToDataTable(
                [
                    ['Time of Day',
                        'Mathématique', ['role' => 'annotation'],
                        'Science', ['role' => 'annotation'],
                        'Culture Générale', ['role' => 'annotation'],
                        'Langue', ['role' => 'annotation'],
                    ],
                    [
                        ['v' => [], 'f' => ''],
                        (isset($result["math"]) ? intval($result["math"]):0),  (isset($result["math"]) ? $result["math"]:0),
                        (isset($result["science"]) ? intval($result["science"]):0),  (isset($result["science"]) ? $result["science"]:0),
                        (isset($result["culture"]) ? intval($result["culture"]):0),  (isset($result["culture"]) ? $result["culture"]:0),
                        (isset($result["langue"]) ? intval($result["langue"]):0),  (isset($result["langue"]) ? $result["langue"]:0),
                    ]
                ]
            );
            $col->getOptions()->setTitle('Progession');
            $col->getOptions()->getAnnotations()->setAlwaysOutside(true);
            $col->getOptions()->getAnnotations()->getTextStyle()->setFontSize(14);
            $col->getOptions()->getAnnotations()->getTextStyle()->setColor('#000');
            $col->getOptions()->getAnnotations()->getTextStyle()->setAuraColor('none');
            $col->getOptions()->getHAxis()->getViewWindow()->setMin([]);
            $col->getOptions()->setWidth(900);
            $col->getOptions()->setHeight(500);

            return $this->render('@AllForKidsMain/Accueil/test.html.twig',array('chart' => $col,'enfants'=>$enfants,'params'=>$params));
        }

    }
    public function barAction(Request $request) {
        $enfant = $request->query->get('enfant');
        $theme = $request->query->get('quiz');
        $params = $request->query->all();
        $user = $this->getUser();
        $result = array();
        if (!empty($user)) {
            $em = $this->getDoctrine()->getManager();
            $enfants = $em->getRepository("AllForKidsMainBundle:Enfant")->findBy(array('idParent' => $user));
            $chart = new \CMEN\GoogleChartsBundle\GoogleCharts\Charts\Material\ColumnChart();
            $connection = $em->getConnection();
            $bars = array(['Nom des quizs', 'Total quiz', 'Points retenu']);
            if (!empty($enfant)) {
                //selection tous les quizs dont l'enfant a repondue
                $query_quizs = "select q.nom_quiz,q.total,s.score from quiz q,score s where q.id = s.id_quiz_id and s.id_enfant = ? order by q.total asc";
                $res = $connection->fetchAll($query_quizs, array($enfant));

                foreach ($res as $item) {
                    $bars[] = [$item['nom_quiz'], $item['score'], $item['total']];
                }
            }
            $chart->getData()->setArrayToDataTable($bars);

            $chart->getOptions()->getChart()
                ->setTitle('Progression');
            $chart->getOptions()
                ->setBars('vertical')
                ->setHeight(400)
                ->setWidth(900)
                ->setColors(['#1b9e77', '#d95f02', '#7570b3'])
                ->getVAxis()
                ->setFormat('decimal');



            return $this->render('@AllForKidsMain/Accueil/bar.html.twig', array('chart' => $chart, 'enfants' => $enfants, 'params' => $params));
        }
        else{
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
    }

}
