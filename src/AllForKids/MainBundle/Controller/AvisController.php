<?php

namespace AllForKids\MainBundle\Controller;

use AllForKids\MainBundle\Entity\Avis;
use AllForKids\MainBundle\Form\AvisType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AvisController extends Controller
{
    public function AjoutAvisAction(Request $request)
    {

        $id = $request->get('id');
        $idU = $request->get('idU');
        $em = $this->getDoctrine()->getManager();
        $av = new Avis();
        $p = $em->getRepository('AllForKidsMainBundle:User')->find($idU);
        $av->setIdParent($p);
        $b = $em->getRepository('AllForKidsMainBundle:User')->find($id);
        $av->setIdBabysitter($b);
        $form = $this->createForm(AvisType::class,$av);
        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM AllForKidsMainBundle:Avis u WHERE u.idBabysitter=:S '
            )->setParameter('S', $id);
        $avis = $query->getResult();

        $form->handleRequest($request);
        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            $em->persist($av);
            $em->flush();
            return $this->redirectToRoute('AjoutAvis',array('id'=>$id ,'idU'=>$idU));

        }

        return $this->render('@AllForKidsMain/Avis/affiche_avis.html.twig', array("Form"=>$form->createView(),
            "Avis"=>$avis,"baby"=>$b
        ));
    }

    public function UpdateAvisAction(Request $request,$idA)
    {
        $id = $request->get('id');

        $em = $this->getDoctrine()->getManager();
        $avis = $em->getRepository("AllForKidsMainBundle:Avis")
            ->find($id);

        $a= $em->getRepository('AllForKidsMainBundle:Avis')->find($idA);
        if ($a=$avis ) {
            $form = $this->createForm(AvisType::class, $avis);
            $form->handleRequest($request);
            if ($form->isValid()) {
              echo $request ->get('idb');

                $em->persist($avis);
                $em->flush();
                return $this->redirectToRoute('AfficheAvis',array('id'=>$request->get('idb')));
            }
        }
        return $this->render("@AllForKidsMain/Avis/ajout_avis.html.twig"
            ,array("Form"=>$form->createView()));
    }

    public function removeAvisAction(Request $request)
    {
        $id = $request->get('id');
        $idb = $request->get('idb');
        $idU = $request->get('idU');
        $em = $this->getDoctrine()->getManager();
        $avis = $em->getRepository("AllForKidsMainBundle:Avis")->find($id);
        $em->remove($avis);
        $em->flush();
        return $this->redirectToRoute('AjoutAvis',array('id'=>$idb ,'idU'=>$idU));
    }

    public function AfficheAvisAction($id)
    {
        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM AllForKidsMainBundle:Avis u WHERE u.idBabysitter=:S '
            )->setParameter('S', $id);
        $avis = $query->getResult();

        return $this->render('@AllForKidsMain/Avis/affiche_avis.html.twig', array(
            "Avis"=>$avis
        ));
    }

    public function ListeAvisAction()
    {
        $em = $this->getDoctrine()->getManager();
        $avis = $em->getRepository("AllForKidsMainBundle:Avis")->findAll();
        return $this->render('AllForKidsMainBundle:Admin:AfficheAvis.html.twig',
            array(
                'avis' => $avis
            ));
    }
    public function removeAnAvisAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $avis = $em->getRepository("AllForKidsMainBundle:Avis")->find($id);
        $em->remove($avis);
        $em->flush();
        return $this->redirectToRoute('ListeAvisAdmin');

    }

}
