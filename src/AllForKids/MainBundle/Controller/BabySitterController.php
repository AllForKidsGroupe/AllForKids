<?php

namespace AllForKids\MainBundle\Controller;

use AllForKids\MainBundle\Entity\User;
use AllForKids\MainBundle\Form\Profile2Type;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BabySitterController extends Controller
{
    public function ShowAction(Request $request,$id)
    {

        $em = $this->getDoctrine()->getManager();
        $babysitter = $em->getRepository("AllForKidsMainBundle:User")->find($id);
        $form = $this->createForm(Profile2Type::class, $babysitter);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $babysitter->UploadProfilePicture();
            $em->persist($babysitter);
            $em->flush();
            //return $this->redirectToRoute('liste');
        }

        return $this->render('@AllForKidsMain/BabySitter/profile2.html.twig', array("Form" => $form->createView()
        ));
    }

    public function listAction()
    {


        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM AllForKidsMainBundle:User u WHERE u.roles LIKE :role'
            )->setParameter('role', '%"ROLE_BABYSITER"%'
            );
        $babysitter = $query->getResult();

            //$babysitter = $em->getRepository("AllForKidsMainBundle:User")
               // ->findBy(array('roles' =>'ROLE_BABYSITER'));


        return $this->render('@AllForKidsMain/BabySitter/listeBabysitter.html.twig', array(
            "babysitter"=>$babysitter
        ));
    }


    public function listAllBabyAction()
    {


        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM AllForKidsMainBundle:User u WHERE u.roles LIKE :role'
            )->setParameter('role', '%"ROLE_BABYSITER"%'
            );
        $babysitter = $query->getResult();


        return $this->render('@AllForKidsMain/Admin/ListeBaby.html.twig', array(
            "babysitter"=>$babysitter
        ));
    }

    public function removebabyAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $babysitter = $em->getRepository("AllForKidsMainBundle:User")->find($id);
        $em->remove($babysitter);
        $em->flush();
        return $this->redirectToRoute('ListeBabyAdmin');


    }


}
