<?php

namespace AllForKids\MainBundle\Controller;

use AllForKids\MainBundle\Entity\Offre;
use AllForKids\MainBundle\Form\OffreType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class OffreController extends Controller
{
    public function ListeOffreAction()
    {
        $em = $this->getDoctrine()->getManager();
        $offre = $em->getRepository("AllForKidsMainBundle:Offre")
            ->findAll();
        return $this->render('@AllForKidsMain/Offre/affiche_Offre.html.twig', array(
            "Offre"=>$offre
        ));
    }

    public function AjoutOffreAction(Request $request,$id,$idU)
    {
        $em = $this->getDoctrine()->getManager();
        $offre = new Offre();
        $p = $em->getRepository('AllForKidsMainBundle:User')->find($idU);
        $offre->setParent($p);
        $b = $em->getRepository('AllForKidsMainBundle:User')->find($id);
        $offre->setBabysitter($b);
        $form = $this->createForm(OffreType::class,$offre);

        $form->handleRequest($request);
        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            $em->persist($offre);
            $em->flush();
            return $this->redirectToRoute('Liste_Offre');

        }
        return $this->render('@AllForKidsMain/Offre/ajout_offre.html.twig', array("Form"=>$form->createView()
        ));
    }

    public function RemoveOffreAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $offre = $em->getRepository("AllForKidsMainBundle:Offre")->find($id);
        $em->remove($offre);
        $em->flush();
        return $this->redirectToRoute('Liste_Offre');
    }

    public function RechercheOffreAction(Request $request)
    {
        $search =$request->query->get('offre');
        $en = $this->getDoctrine()->getManager();
        $offre=$en->getRepository("AllForKidsMainBundle:Offre")->findville($search);

        return $this->render('AllForKidsMainBundle:Offre:affiche_Offre.html.twig', array(
            'Offre' => $offre
        ));
    }

    public function ModifierOffreAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $offre = $em->getRepository("AllForKidsMainBundle:Offre")
            ->find($id);
        $form = $this->createForm(OffreType::class,$offre);
        $form->handleRequest($request);
        if($form->isValid())
        {
            $em->persist($offre);
            $em->flush();
            return $this->redirectToRoute('Liste_Offre');
        }

        return $this->render("@AllForKidsMain/Offre/ajout_offre.html.twig"
            ,array("Form"=>$form->createView()));
    }

    public function AcceptOffreAction(Request $request){
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $offre = $em->getRepository("AllForKidsMainBundle:Offre")
            ->find($id);
        $offre->setAccept(1);
        $em->flush($offre);

        $offre = $em->getRepository("AllForKidsMainBundle:Offre")
            ->findAll();
        return $this->render('@AllForKidsMain/Offre/affiche_Offre.html.twig', array(
            "Offre"=>$offre
        ));

    }

}
