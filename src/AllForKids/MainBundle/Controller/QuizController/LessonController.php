<?php

namespace AllForKids\MainBundle\Controller\QuizController;

use AllForKids\MainBundle\Entity\Lesson;
use AllForKids\MainBundle\Form\LessonType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class LessonController extends Controller
{
    /**
     * @Route("/AjouterLesson")
     */
    public function AjouterLessonAction(Request $request)
    { $Lesson = new Lesson();
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {
            $data = $request->request->all();

            $Lesson->setBrochure($request->files->get('joint'));


            $file = $Lesson->getBrochure();

            $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

            // moves the file to the directory where brochures are stored
            $chemain = $this->get('kernel')->getRootDir() . '/../web/upload/pdfs';
            $file->move(
                $chemain,
                $fileName
            );

            $Lesson->setBrochure($fileName);
            $Lesson->setTheme($data['theme']);
            $Lesson->setCategorieAge($data['category']);
            $now = new \DateTime();
            $Lesson->setDate($now);
            $em->persist($Lesson);
            $em->flush();

        }
        return $this->render('@AllForKidsMain/Lesson/ajouter_lesson.html.twig', array());
    }
    private function generateUniqueFileName()
    {

        return md5(uniqid());
    }
    public function ListeAction(Request $request)
    {
        $lesson = new Lesson();


        if ($request->isMethod('POST')) {

            $em = $this->getDoctrine()->getManager();

            $theme = $request->get('theme');
            $category = $request->get('category');
            var_dump($theme);

            if ($category != null) {

                $lesson= $em->getRepository("AllForKidsMainBundle:Lesson")->findBy(array('categorieAge' => $category));

            } else
                if ($theme != null) {


                    $lesson = $em->getRepository("AllForKidsMainBundle:Lesson")->findBy(array('theme' => $theme));


                } else
                    if ($theme != null && $category != null) {


                        $lesson = $em->getRepository("AllForKidsMainBundle:Lesson")->RechercheLesson($theme, $category);

                    } else {

                        $em = $this->getDoctrine()->getManager();
                        $lesson = $em->getRepository("AllForKidsMainBundle:Lesson")->findAll();

                    }


        }

        return $this->render('@AllForKidsMain/Lesson/AfficherLesson.html.twig'
            , array('lesson' => $lesson)

        );
    }
    public function DeleteAction(Request $request,$id)
    {
        $em=$this->getDoctrine()->getManager();
        $quiz=$em->getRepository("AllForKidsMainBundle:Lesson")->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($quiz);
        $em->flush();
        return $this->redirectToRoute('AfficheLesson');
    }


}
