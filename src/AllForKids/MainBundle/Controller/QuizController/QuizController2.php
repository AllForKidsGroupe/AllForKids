<?php

namespace AllForKids\MainBundle\Controller\QuizController;

use AllForKids\MainBundle\Entity\Score;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AllForKids\MainBundle\Entity\Quiz;
use AllForKids\MainBundle\Entity\Question;
use AllForKids\MainBundle\Entity\Reponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use CMEN\GoogleChartsBundle\GoogleCharts\Charts\ColumnChart;
class QuizController2 extends Controller
{
    public function AjouterQuizAction(Request $request)
    {

        if ($request->isMethod('POST')) {
            $total=0;
            // on fait appel à EntityManager
            $em = $this->getDoctrine()->getManager();
            //on récupère tous les valeurs des champs de notre formulaire
            $image = $request->files->get('image');
            //var_dump($request->files);exit();
            $data = $request->request->all();

            //var_dump($data);exit();
            // instance de l'entité Quiz
            $quiz = new Quiz();
            $quiz->setNomquiz($data['quiz']);
            $quiz->setCategorieage($data['category']);
            $quiz->setTheme($data['theme']);




            $stored_image = $this->uploadFiles($image);
            $quiz->setImage($stored_image);

            $quiz->setDescription($data['description']);
            $quiz->setTime($data['time']);

            $em->persist($quiz);
            // ici on parcours les champs viens du formulaire
            // on test si les champs ne sont pas vides
            if(isset($data['question']) && !empty($data['question'])){
                //on parcours toutes les questions
                foreach($data['question'] as $key=>$question_libelle){
                    $question = new Question();
                    $question->setLibelle($question_libelle);
                    $question->setIdQuiz($quiz);
                    $em->persist($question);
                    $reponses = $data['reponse'][$key];
                    if(isset($data['verif'][$key])) {
                        $verifs = $data['verif'][$key];
                    }
                    $points = $data['point'][$key];
                    //var_dump($reponses,$verifs);exit();

                    foreach($reponses as $index=>$reponse_libelle){
                        $reponse = new Reponse();
                        if(isset($verifs[$index]) && !empty($verifs[$index])){
                            $current_verif = $verifs[$index];
                        }else{
                            $current_verif = "0" ;
                        }
                        if(isset($points[$index]) && !empty($points[$index])){
                        $p=$points[$index];
                            $total+=$points[$index];
                        }

                        $reponse->setLibelle($reponse_libelle);
                        $reponse->setPoint($p);
                        $reponse->setVerif($current_verif);
                        $reponse->setIdQuest($question);

                    }

                }

            }
            $quiz->setTotal($total);
            $em->persist($reponse);
            $em->flush();
            return $this->redirectToRoute('AfficheQuiz');
        }


        return $this->render('@AllForKidsMain/Quiz/AjouterQuiz.html.twig'
           )

        // , array('form'=>$Form->createView())
            ;


    }

    public function uploadFiles($file){
        if (null === $file) {
            return;
        }
        $chemain = $this->get('kernel')->getRootDir() . '/../web/upload';
        $stored_image = $file->getClientOriginalName();
        $file->move($chemain,$stored_image);
        return $stored_image;
    }

    public function GetAllQuizAction(Request $request)
    {
        $q = new Quiz();


        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();

            $theme = $request->get('theme');
            $category = $request->get('category');

            if ($category != null) {

                $serialzier = new Serializer((array(new ObjectNormalizer())));
                $quiz = $em->getRepository("AllForKidsMainBundle:Quiz")->findBy(array('categorieAge' => $category));
                $q = $serialzier->normalize($quiz);
                return new JsonResponse($q);
            } else
                if ($theme != null) {

                    $serialzier = new Serializer((array(new ObjectNormalizer())));
                    $quiz = $em->getRepository("AllForKidsMainBundle:Quiz")->findBy(array('theme' => $theme));
                    $q = $serialzier->normalize($quiz);
                    return new JsonResponse($q);

                }
                else
                    if ($theme != null && $category!=null) {

                        $serialzier = new Serializer((array(new ObjectNormalizer())));
                        $quiz = $em->getRepository("AllForKidsMainBundle:Quiz")->RechercheQuiz($theme,$category);
                        $q = $serialzier->normalize($quiz);
                        return new JsonResponse($q);

                    }

                    else {
                    $serialzie = new Serializer((array(new ObjectNormalizer())));
                    $em = $this->getDoctrine()->getManager();
                    $quiz = $em->getRepository("AllForKidsMainBundle:Quiz")->findAll();
                    $q = $serialzie->normalize($quiz);
                    return new JsonResponse($q);
                }

            }

            return $this->render('@AllForKidsMain/Quiz/AfficheQuiz.twig'
                , array('quiz' => $q)

            );

    }
    public function DisplayAction(Request $request)
    {
        $user=$this->get('security.token_storage')->getToken()->getUser();
        $id=$user->getId();
        $theme=$request->get('theme');
        var_dump($theme);
        $em=$this->getDoctrine()->getManager();
        $Enfant = $em->getRepository("AllForKidsMainBundle:Enfant")->find(1);

        $Quiz=new Quiz();
        $now = new \DateTime();
        $anneAct=$now->format('m/d/Y');
         $anne=substr($Enfant->getDateNaissance()->format('m/d/Y'),6);

        $age=substr($anneAct,6)-$anne;

        if($age>3 && $age<=6)
        {$catage='4-6';
            $Quiz=$em->getRepository("AllForKidsMainBundle:Quiz")->findBy(array('categorieAge'=>'4-6'));
        }
        else
            if($age>6 && $age<=9)
            {$catage='7-9';
                $Quiz=$em->getRepository("AllForKidsMainBundle:Quiz")->findBy(array('categorieAge'=>'7-9'));
            }
            else
                if($age>9 && $age<=12)
                {$catage='10-12';
                    $Quiz=$em->getRepository("AllForKidsMainBundle:Quiz")->findBy(array('categorieAge'=>'9-12'));
                }
                else
                    if($age>12 && $age<=15)
                    {$catage='13-15';
                    $Quiz=$em->getRepository("AllForKidsMainBundle:Quiz")->findBy(array('categorieAge'=>'13-15'));
                    }
                    else {

                     $Quiz = $em->getRepository("AllForKidsMainBundle:Quiz")->findAll();
                    }
                    if($theme!=null)
                    {
                        $Quiz = $em->getRepository("AllForKidsMainBundle:Quiz")->RechercheQuiz($theme,$catage) ;
                    }


        return $this->render('@AllForKidsMain/Quiz/DisplayQuizez.html.twig'
            , array('quiz'=>$Quiz,'enfant'=>$id)

        );
    }
    public function UpdateQuizAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $quiz = $em->getRepository("AllForKidsMainBundle:Quiz")->find($id);
        $questions =$em->getRepository("AllForKidsMainBundle:Question")->findBy(array('idQuiz' => $quiz->getId()));


        foreach ($questions as $item) {
            $reponse = $em->getRepository("AllForKidsMainBundle:Reponse")->findBy(array('idQuest' => $item->getId()));
            $item->setReponses($reponse);
        }

        if ($request->isMethod('POST')) {
            $total=0;
            $referer = $request->headers->get('referer');
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            /*print_r($questions);
            print_r($data);exit();*/
            $quiz->setNomquiz($data['quiz']);
            $quiz->setCategorieage($data['category']);
            $quiz->setTheme($data['theme']);
            if(isset($data['question'])){
                //on parcours toutes les questions
                foreach($data['question'] as $key=>$question_libelle){
                    $idquestion = $data['idquestion'][$key];
                    if(!empty($idquestion))
                        $question = $em->getRepository("AllForKidsMainBundle:Question")->find($idquestion);
                    else
                        $question = new Question();
                    $question->setLibelle($question_libelle);
                    if(empty($idquestion)){
                        $question->setIdQuiz($quiz);
                        $em->persist($question);
                    }
                    $reponses = $data['reponse'][$key];
                    $verifs = isset($data['verif'][$key]) ? $data['verif'][$key] : null;
                    $points = isset($data['point'][$key]) ? $data['point'][$key] : null;
                    $ids_reponse = isset($data['idreponse'][$key]) ? $data['idreponse'][$key] : null;
                    //
                    //var_dump($reponses,$ids_reponse);exit();
                    foreach($reponses as $index=>$reponse_libelle){
                        $idreponse = isset($ids_reponse[$index]) ? $ids_reponse[$index] : null;
                        if(!empty($idreponse))
                            $reponse = $em->getRepository("AllForKidsMainBundle:Reponse")->find($idreponse);
                        else{
                            $reponse = new Reponse();
                        }


                        if(isset($verifs[$index])){
                            $current_verif = $verifs[$index];
                        }else{
                            $current_verif = "0" ;
                        }
                        if(isset($points[$index])){
                            $current_points=$points[$index];
                            $total+=$points[$index];
                        }
                        $reponse->setLibelle($reponse_libelle);
                        $reponse->setVerif($current_verif);
                        $reponse->setPoint($current_points);
                        if(empty($idreponse)){
                            $reponse->setIdQuest($question);
                            $em->persist($reponse);
                        }
                    }
                }

                //detection si une question est supprimée
                $question_inputs = $data['idquestion'];
                foreach($questions as $item){

                    //detection si une question est supprimée
                    if(!in_array($item->getId(),$question_inputs)){
                        $em->remove($item);
                    }
                    else{
                        //detection si une réponse est supprimée de la uestion courant
                        $key_question = array_search ($item->getId(),$question_inputs);
                        if(isset($data['idreponse'][$key_question])){
                            foreach($item->getReponses() as $item_reponse){
                                if(!in_array($item_reponse->getId(),$data['idreponse'][$key_question]))
                                    $em->remove($item_reponse);
                            }
                        }

                    }


                }
            }
            $quiz->setTotal($total);
            //exit();
            $em->flush();

            return $this->redirectToRoute('AfficheQuiz');

        }

        $quiz->setQuestions($questions);
        return $this->render('@AllForKidsMain/Quiz/UpdateQuiz.html.twig',array('quiz'=>$quiz,'question'=>$questions));

    }
    public function JouerQuizAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $quiz = $em->getRepository("AllForKidsMainBundle:Quiz")->find($id);
        $question=$em->getRepository("AllForKidsMainBundle:Question")->findBy(array('idQuiz' => $quiz->getId()));
        $somme=0;



        foreach ($question as $item) {
            $reponse = $em->getRepository("AllForKidsMainBundle:Reponse")->findBy(array('idQuest' => $item->getId()));

            $item->setReponses($reponse);

        }

        $quiz->setQuestions($question);
        $score=new Score();
        $score->setIdQuiz($quiz);
        if ($request->isMethod('POST')) {

            $em = $this->getDoctrine()->getManager();

            $data = $request->request->all();


            if(isset($data['question']) && !empty($data['question']))
            {
                //on parcours toutes les questions
                foreach($data['question'] as $key=>$question_libelle){


                    $reponses = $data['reponse'][$key];
                    if(isset($data['verif'][$key]) && isset($data['veri'][$key]) && isset($data['point'][$key]) ) {
                        $verifs = $data['verif'][$key];
                        $verifss = $data['veri'][$key];
                        $points = $data['point'][$key];
                        foreach($reponses as $index=>$reponse_libelle){

                            if(isset($verifs[$index]) && isset($verifss[$index])) {
                               if($verifs[$index]==$verifss[$index])
                               {
                                   if(isset($points[$index])){
                                   $somme=$somme+$points[$index];}
                               }
                            }
                        }

                    }

                }
                $score->setIdQuiz($quiz);
                $score->setScore($somme);
                $em->persist($score);
                $em->flush();
            }
            return $this->redirectToRoute('DisplayQuizeze');
        }


        return $this->render('@AllForKidsMain/Quiz/JouerQuiz.twig'

            , array('quiz'=>$quiz,'question'=>$question,'reponse'=>$reponse)
        );
    }
    public function DeleteAction(Request $request,$id)
    {
        $em=$this->getDoctrine()->getManager();
        $quiz=$em->getRepository("AllForKidsMainBundle:Quiz")->find($id);


        $em = $this->getDoctrine()->getManager();
        $em->remove($quiz);
        $em->flush();
        return $this->redirectToRoute('AfficheQuiz');
    }
    public function verificationAction(Request $request)
    {

        $tchiffres=array(0,1,2,3,4,5,6,7,8,9,10,12);
        $tlettres=array("zéro","un","deux","trois","quatre","cinq","six","sept","huit","neuf","dix","onze","douze");
        $premier=rand ( 0 , count($tchiffres)-1 );
        $second=rand ( 0 , count($tchiffres)-1 );
        $choixsigne=rand ( 0 ,1 );
        if($second<=$premier && $choixsigne==1 )
        {
            $resultat=md5($tchiffres[$premier]-$tchiffres[$second]);
            $operation="combien font ".$tlettres[$premier]." retranché de ".$tlettres[$second]." ?";
        }
        else if($second<=$premier && $choixsigne==0 )
        {
            $resultat=md5($tchiffres[$premier]-$tchiffres[$second]);
            $operation="combien font ".$tlettres[$premier]." moins ".$tlettres[$second]." ?";
        }
        else if ($second>$premier && $choixsigne==1 )
        {
            $resultat=md5($tchiffres[$premier]+$tchiffres[$second]);
            $operation="combien font ".$tlettres[$premier]." ajouté à ".$tlettres[$second]." ?";

        }
        else
        {
            $resultat=md5($tchiffres[$premier]+$tchiffres[$second]);
            $operation="combien font ".$tlettres[$premier]." plus ".$tlettres[$second]." ?";

        }
        if ($request->isMethod('POST')) {
            $reponsecap = $request->request->get("reponsecap");
            $reponsecapcode = $request->request->get("reponsecapcode");
            if (md5(htmlspecialchars($reponsecap))==htmlspecialchars($reponsecapcode)){
                return $this->redirectToRoute('site_quiz_bravo');
            }else{
                return $this->redirectToRoute('site_quiz_verify');
            }
        }
        return $this->render('@AllForKidsMain/Quiz/verify.html.twig',array('operation'=>$operation,'resultat'=>$resultat));
    }

    public function bravoAction(Request $request)
    {

        return $this->render('@AllForKidsMain/Quiz/bravo.html.twig');
    }
    public function calculateAge($dateOfBirth) {
        $today = date("Y-m-d");
        $diff = date_diff(date_create($dateOfBirth), date_create($today));
        return $diff->format('%y');
    }

    public function checkidAction(Request $request) {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->request->get('enfant');
            $Enfant = $em->getRepository("AllForKidsMainBundle:Enfant")->find($id);
            $age = $this->calculateAge($Enfant->getDateNaissance()->format('d-m-Y'));
            if ($age > 10){
                $data = $this->verificationAction();
                return $this->render('@AllForKidsMain/Quiz/captcha.html.twig',$data);
            }
            else
                echo "0";

        }
    }
    public function chartAction(Request $request)
    {
        $enfant = $request->query->get('enfant');
        $theme = $request->query->get('theme');
        $params = $request->query->all();
        $themes = array("math"=>'Mathématique',"science"=>'Science',"culture"=>'Culture Générale',"langue"=>'Langue');
        $user = $this->getUser();
        $result = array();
        if(!empty($user)){
            $em = $this->getDoctrine()->getManager();
            $enfants = $em->getRepository("AllForKidsMainBundle:Enfant")->findBy(array('idParent'=>$user));
            if(empty($theme)){
                foreach ($themes as $key => $label) {
                    $sql = "select sum(score) from score where id_enfant = ? and id_quiz_id in (select id from quiz where theme = ? )";
                    $score = $em->getConnection()->fetchColumn($sql, array($enfant,$label), 0);
                    $result[$key] = $score;
                }

            }else{
                $key = array_search($theme,$themes);
                $sql = "select sum(score) from score where id_enfant = ? and id_quiz_id in (select id from quiz where theme = ? )";
                $result[$key] = $em->getConnection()->fetchColumn($sql, array($enfant,$theme), 0);
            }
            $barTheme[] = ['v' => [], 'f' => ''];
            foreach ($themes as $key => $value) {
                if(!array_key_exists($key,$result)){
                    $barTheme[] = 0;
                    $barTheme[] = '0';
                }else{
                    if($result[$key] == null)
                        $result[$key] = '0';
                    $barTheme[] = $result[$key];
                    $barTheme[] = $result[$key];
                }
            }
            //var_dump($barTheme);exit();
            $col = new ColumnChart();
            $col->getData()->setArrayToDataTable(
                [
                    ['Time of Day',
                        'Mathématique', ['role' => 'annotation'],
                        'Science', ['role' => 'annotation'],
                        'Culture Générale', ['role' => 'annotation'],
                        'Langue', ['role' => 'annotation'],
                    ],
                    [
                        ['v' => [], 'f' => ''],
                        (isset($result["math"]) ? intval($result["math"]):0),  (isset($result["math"]) ? $result["math"]:0),
                        (isset($result["science"]) ? intval($result["science"]):0),  (isset($result["science"]) ? $result["science"]:0),
                        (isset($result["culture"]) ? intval($result["culture"]):0),  (isset($result["culture"]) ? $result["culture"]:0),
                        (isset($result["langue"]) ? intval($result["langue"]):0),  (isset($result["langue"]) ? $result["langue"]:0),
                    ]
                ]
            );
            $col->getOptions()->setTitle('Progession');
            $col->getOptions()->getAnnotations()->setAlwaysOutside(true);
            $col->getOptions()->getAnnotations()->getTextStyle()->setFontSize(14);
            $col->getOptions()->getAnnotations()->getTextStyle()->setColor('#000');
            $col->getOptions()->getAnnotations()->getTextStyle()->setAuraColor('none');
            $col->getOptions()->getHAxis()->getViewWindow()->setMin([]);
            $col->getOptions()->setWidth(900);
            $col->getOptions()->setHeight(500);

            return $this->render('@AllForKidsMain/Quiz/test.html.twig',array('chart' => $col,'enfants'=>$enfants,'params'=>$params));
        }

    }
    public function barAction(Request $request)
    {
        $enfant = $request->query->get('enfant');
        $theme = $request->query->get('quiz');
        $params = $request->query->all();
        $user = $this->getUser();
        $result = array();
        if(!empty($user)){
            $em = $this->getDoctrine()->getManager();
            $enfants = $em->getRepository("AllForKidsMainBundle:Enfant")->findBy(array('idParent'=>$user));
            $chart = new \CMEN\GoogleChartsBundle\GoogleCharts\Charts\Material\ColumnChart();
            $connection = $em->getConnection();
            $bars = array(['Nom des quizs', 'Total quiz', 'Points retenu']);
            if(!empty($enfant)){
                //selection tous les quizs dont l'enfant a repondue
                $query_quizs = "select q.nom_quiz,q.total,s.score from quiz q,score s where q.id = s.id_quiz_id and s.id_enfant = ? order by q.total asc";
                $res = $connection->fetchAll($query_quizs, array($enfant));

                foreach($res as $item){
                    $bars[] = [$item['nom_quiz'], $item['score'], $item['total']];
                }
            }
            $chart->getData()->setArrayToDataTable($bars);

            $chart->getOptions()->getChart()
                ->setTitle('Progression');
            $chart->getOptions()
                ->setBars('vertical')
                ->setHeight(400)
                ->setWidth(900)
                ->setColors(['#1b9e77', '#d95f02', '#7570b3'])
                ->getVAxis()
                ->setFormat('decimal');



            return $this->render('@AllForKidsMain/Quiz/bar.html.twig',array('chart' => $chart,'enfants'=>$enfants,'params'=>$params));
        }

    }

}