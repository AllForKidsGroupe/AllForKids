<?php

namespace AllForKids\MainBundle\Controller\QuizController;

use AllForKids\MainBundle\Entity\MediaVideo;
use AllForKids\MainBundle\Form\MediaVideoType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
class VideoController extends Controller
{
    public function AjouterAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $video=new MediaVideo();
        if ($request->isMethod('POST')) {
            // on fait appel à EntityManager

            //on récupère tous les valeurs des champs de notre formulaire
            $data = $request->request->all();

            $video->setUrl($data['url']);
            $video->setTheme($data['theme']);
            $video->setCategorieage($data['category']);
            $now = new \DateTime();
            $video->setDate($now);
            $video->setDescription($data['description']);
            $video->extractIdentif();
            $em->persist($video);
        }

                $em->flush();
        $this->redirectToRoute('AfficheVideo');

            return $this->render('@AllForKidsMain/Video/Ajoutvideo.html.twig'
                , array()

            );
        }


        public function ListeAction(Request $request)
        {
            $video = new MediaVideo();

            $em = $this->getDoctrine()->getManager();
            $video = $em->getRepository("AllForKidsMainBundle:MediaVideo")->findAll();
            if ($request->isMethod('POST')) {

                $em = $this->getDoctrine()->getManager();

                $theme = $request->get('theme');
                $category = $request->get('category');
                var_dump($theme);

                if ($category != null) {

                  //  $serialzier = new Serializer((array(new ObjectNormalizer())));
                    $video = $em->getRepository("AllForKidsMainBundle:MediaVideo")->findBy(array('categorieAge' => $category));
                   // $q = $serialzier->normalize($quiz);
                   // return new JsonResponse($q);
                } else
                    if ($theme != null) {

                     //   $serialzier = new Serializer((array(new ObjectNormalizer())));
                        $video = $em->getRepository("AllForKidsMainBundle:MediaVideo")->findBy(array('theme' => $theme));
                       // $q = $serialzier->normalize($quiz);
                        //return new JsonResponse($q);

                    } else
                        if ($theme != null && $category != null) {

                           // $serialzier = new Serializer((array(new ObjectNormalizer())));
                            $video = $em->getRepository("AllForKidsMainBundle:MediaVideo")->RechercheVideo($theme, $category);
                           // $q = $serialzier->normalize($quiz);
                           // return new JsonResponse($q);

                        } else {
                          //  $serialzie = new Serializer((array(new ObjectNormalizer())));
                            $em = $this->getDoctrine()->getManager();
                            $video = $em->getRepository("AllForKidsMainBundle:MediaVideo")->findAll();
                          //  $q = $serialzie->normalize($quiz);
                           // return new JsonResponse($q);
                        }

            }

            return $this->render('@AllForKidsMain/Video/AfficheVideo.html.twig'
                , array('video' => $video)

            );
        }
    public function DeleteAction(Request $request,$id)
    {
        $em=$this->getDoctrine()->getManager();
        $quiz=$em->getRepository("AllForKidsMainBundle:MediaVideo")->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($quiz);
        $em->flush();
        return $this->redirectToRoute('AfficheVideo');
    }

    public function DisplayAction(Request $request)
    {
        $user=$this->get('security.token_storage')->getToken()->getUser();
        $id=$user->getId();


        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository("AllForKidsMainBundle:MediaVideo")->findAll();
        if ($request->isMethod('POST')) {
            $theme=$request->get('theme');
           $em = $this->getDoctrine()->getManager();
            $connection = $em->getConnection();
            $sql = "select * from mediavideo where  theme='$theme' and SUBSTRING_INDEX(SUBSTRING_INDEX(categorieage, '-', 1), '-', -1) >=
	     all(SELECT extract(year from CURRENT_DATE)-extract(year from datenaissance) from enfant where id_parent_id=$id)";
            $video = $connection->fetchAll($sql, array());
            var_dump($video);


        }

        return $this->render('@AllForKidsMain/Video/DisplayVideo.html.twig'
            , array('video' => $video)

        );
    }
}