<?php

namespace AllForKids\MainBundle\Controller;

use AllForKids\MainBundle\Entity\evenement;
use AllForKids\MainBundle\Entity\Reservation;
use AllForKids\MainBundle\Form\evenementType;
use AllForKids\MainBundle\Form\nombreTicketType;
use AllForKids\MainBundle\Form\ReservationType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReservationController extends Controller
{


    public function ListerReservationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $reserv = $em->getRepository("AllForKidsMainBundle:Reservation")->findAll();
        return $this->render('@AllForKidsMain/Reservation/ListeReservation.html.twig',
            array(
                'reservation' => $reserv
            ));

    }

    public function ReservationAdminAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $reserv = $em->getRepository("AllForKidsMainBundle:Reservation")->findAll();
        return $this->render('@AllForKidsMain/Reservation/listeReservationAdmin.html.twig',
            array(
                'reservation' => $reserv,
            ));

    }




    public function reservAction(Request $request, $id_even)
    {
        $r = new Reservation();
        $em = $this->getDoctrine()->getManager();
        $parent = $this->getUser()->getId();
        if ($request->isMethod('post')) {

            $em = $this->getDoctrine()->getManager();
            $even = $em->getRepository('AllForKidsMainBundle:evenement')->find($id_even);
            if ($even->getTicketDisponible() > 0) {
                $r->setNbreTicket($request->get('nbr'));
                if ($request->get('nbr') < $even->getTicketDisponible()) {


                    // $hh = $form->get('nbreTicket')->getData();
                    $r->setIdClient($parent);
                    $r->setDateReservation(new \DateTime('now'));

                    $r->setIdEven($even->getIdEven());

                    $em->persist($r);

                    $em->flush();

                    $tiketdispo=$even->getTicketDisponible()-$request->get('nbr');
                 $even->setTicketDisponible($tiketdispo);

                    $em->persist($even);

                    $em->flush();


                    //  $pers=$em->getRepository('MyAppUserBundle:User')->findOneBy(['id'=>$even->getIdUser()]);
                    return $this->redirectToRoute('listeEven');
                }
                else {

                    return $this->redirectToRoute('listeEven');

                }

            } else {

                return $this->redirectToRoute('listeEven');
            }

        }


    }












}