<?php

namespace AllForKids\MainBundle\Controller;

use AllForKids\MainBundle\Entity\LigneCommandes;
use AllForKids\MainBundle\Entity\Produits;
use AllForKids\MainBundle\Entity\User;
use AllForKids\MainBundle\Form\ProduitsType;
use AllForKids\MainBundle\Form\RechercherType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class StoreController extends Controller
{
    public function AjoutAction(Request $request)
    {
        $p = new Produits();
        $form = $this->createForm(ProduitsType::class, $p);
        $form->handleRequest($request);/*creation d'une session pr stocker les valeurs de l'input*/
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $p->getImage();
            $filename = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move(
                $this->getParameter('Produitsdirectory'),
                $filename
            );
            $p->setImage($filename);
            $em = $this->getDoctrine()->getManager();
            $em->persist($p);
            $em->flush();

        }
        return $this->render('@AllForKidsMain/Store/AjoutProd.html.twig', array(
            'form' => $form->createView()
        ));
    }
    public function ajout2Action(Request $request){
        $p = new Produits();
        $form = $this->createForm(ProduitsType::class, $p);
        $form->handleRequest($request);/*creation d'une session pr stocker les valeurs de l'input*/
            if ($form->isSubmitted() && $form->isValid()) {
                $file = $p->getImage();
                $filename = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                    $this->getParameter('Produitsdirectory'),
                    $filename
                );
                $p->setImage($filename);
                $em = $this->getDoctrine()->getManager();
                $em->persist($p);
                $em->flush();

        }
        return $this->render('@AllForKidsMain/Store/Ajout2.html.twig', array(
            'form' => $form->createView()
        ));
    }
    public function ValiderProduitAction(Request $request){
        $idproduit=$request->get('id');
        $em= $this->getDoctrine()->getManager();
        $produit=$em->getRepository("AllForKidsMainBundle:Produits")->findOneById($idproduit);
        $produit->setEtat("accepted");
        $em->persist($produit);
        $em->flush();


        return New Response('Ok');

    }


   public function AfficherProduitAction(Request $request)
    {
        $session= $this->get('session');
      $em = $this->getDoctrine()->getManager();
       $prod = $em->getRepository("AllForKidsMainBundle:Produits")->findAll();
        $paginator  = $this->get('knp_paginator');
        $pagniate = $paginator->paginate(
            $prod, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 6)/*limit per page*/
        );
        $produits=$em->getRepository('AllForKidsMainBundle:Produits')->findArray(array_keys($session->get('panier')));
        return $this->render('@AllForKidsMain/Store/AfficherProduit.twig', array(
           'p'=>$pagniate, 'produits'=>$produits,'panier'=>$session->get('panier'),array('p'=>$prod))
      );

   }
    public function ListeAdminAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $prod = $em->getRepository("AllForKidsMainBundle:Produits")->findAll();
        $paginator  = $this->get('knp_paginator');
        $pagniate = $paginator->paginate(
            $prod, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 7)/*limit per page*/
        );
        return $this->render('@AllForKidsMain/Admin/ListeProduits.html.twig',array(
            'p'=>$pagniate
        ));

    }
    public function ListePrestataiteAction()
    {
        $em = $this->getDoctrine()->getManager();
        $prod = $em->getRepository("AllForKidsMainBundle:Produits")->findAll();
        return $this->render('@AllForKidsMain/Store/LisetProduitsPrestataire.html.twig',array(
            'p'=>$prod
        ));

    }
    function updateAction(Request $request,$id){
        $em=$this->getDoctrine()->getManager();
        $p=$em->getRepository("AllForKidsMainBundle:Produits")->find($id);
        $form=$this->createForm(ProduitsType::class,$p);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $file = $p->getImage();
            $filename = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move(
                $this->getParameter('Produitsdirectory'),
                $filename
            );
            $p->setImage($filename);
            $em->persist($p);
            $em->flush();
        }
        return $this->render('@AllForKidsMain/Admin/Update.html.twig',array(
                'form'=>$form->createView())
        );
    }
    function RechercheAction(Request $request)
    {
        $p= new Produits();
        $form = $this->createForm(RechercherType::class,$p);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if( $form->isSubmitted()) {
            $cat=$p->getCategorie();
            $prod = $em->getRepository("AllForKidsMainBundle:Produits")->DQL($cat);
        }else{
            $prod=$em->getRepository("AllForKidsMainBundle:Produits")->findAll();
        }
        return $this->render('@AllForKidsMain/Admin/Recherche.html.twig',array(
            'form' => $form->createView(), 'p'=>$prod ,
        ));
    }
    public function AjoutAuPanierAction ($id,Request $request)
    {
        $session= $this->get('session');
       //si ma session n'existe pas , on l'initialise avec un tableau (kima isset)
        if (!$session->has('panier')) $session->set('panier',array());

        $panier=$session->get('panier');
        //verifier que l id existe
        if(array_key_exists($id,$panier)) {
            if ($request->query->get('qte') != null) $panier[$id] = ($request->query->get('qte'));
        }else {
            if ($request->query->get('qte') != null)
                $panier[$id] = ($request->query->get('qte'));
            //$panier[$id]=$panier[$id]+1;
            else $panier[$id] = 1;
        }

        $session->set('panier',$panier);


        $em=$this->getDoctrine()->getManager();
        $prod=$em->getRepository("AllForKidsMainBundle:Produits")->findAll();
        $paginator  = $this->get('knp_paginator');
        $pagniate = $paginator->paginate(
            $prod, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $request->query->getInt('limit', 6)/*limit per page*/
        );
        $produits=$em->getRepository('AllForKidsMainBundle:Produits')->findArray(array_keys($session->get('panier')));
        if (!$session->has('panier')) $session->set('panier',array());


        return $this->render('@AllForKidsMain/Store/AfficherProduit.twig',array('p'=>$pagniate,
            'panier'=>$session->get('panier'),'produits'=>$produits));
    }
    public function PanierAction()
    {

        $session= $this->get('session');
        if (!$session->has('panier')) $session->set('panier',array());

        $em=$this->getDoctrine()->getManager();
        $produits=$em->getRepository('AllForKidsMainBundle:Produits')->findArray(array_keys($session->get('panier')));

        return $this->render('@AllForKidsMain/Store/details.html.twig',array('produits'=>$produits,
            'panier'=>$session->get('panier')));
    }
    public function SupprimerDuPanierAction($id){
        $session= $this->get('session');
        $panier=$session->get('panier');
        if(array_key_exists($id,$panier)) {
            unset($panier[$id]);
            $session->set('panier',$panier);

        }
        return $this->redirectToRoute('Details',array());
        }
    public function ajouterLigneAction(Request $request)
    {
        $session= $this->get('session');
        $panier=$session->get('panier');
        if($panier!=NULL) {
            $em = $this->getDoctrine()->getManager();
            $produits = $em->getRepository("AllForKidsMainBundle:Produits")->findArray(array_keys($session->get('panier')));
            $user = $this->getUser();
            $uid=$user->getId();
            foreach ($produits as $p) {
                $l = new LigneCommandes();
                $l->setIdclient($uid);
                $l->setIdProduit($p->getId());
                $l->setPrixTotal( $p->getPrix());
                $l->setNbrArticle($panier[$p->getId()]);
                $n=$panier[$p->getId()];
                $q=$p->getQuantite();
                $p->setQuantite($q-$n);

                $em->persist($l);
                $em->flush();
                /*unset($panier[$p->getId()]);
                $session->set('panier',$panier);*/
            }
           /* $session->remove('panier');*/
            }
        return $this->render('@AllForKidsMain/Store/Paiement.html.twig');

        }
        public function PfdAction(){
            $em = $this->getDoctrine()->getManager();
            $prod = $em->getRepository("AllForKidsMainBundle:Produits")->findAll();
            $snappy = $this->get('knp_snappy.pdf');
            $html = $this->renderView('@AllForKidsMain/Store/LisetProduitsPrestataire.html.twig', array('p'=>$prod
            ));
            $filename = 'ProduitsPDF';

            return new Response(
                $snappy->getOutputFromHtml($html),
                200,
                array(
                    'Content-Type'          => 'application/pdf',
                    'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"'
                )
            );
        }

    public function Pfd2Action(){

        $snappy = $this->get('knp_snappy.pdf');
        $html = $this->renderView('@AllForKidsMain/Store/Paiement.html.twig');
        $filename = 'myFirstSnappyPDF';

        return new Response(
            $snappy->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"'
            )
        );
    }
    function SupprimerAction(Request $request,$id){
        $em=$this->getDoctrine()->getManager();
        $p=$em->getRepository("AllForKidsMainBundle:Produits")->find($id);
        $em->remove($p);
        $em->flush();
        return $this->redirectToRoute('Listes',array());

    }




}
