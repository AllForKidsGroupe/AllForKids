<?php

namespace AllForKids\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Avis
 *
 * @ORM\Table(name="avis")
 * @ORM\Entity(repositoryClass="AllForKids\MainBundle\Repository\AvisRepository")
 */
class Avis
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AllForKids\MainBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $idParent;

    /**
     * @ORM\ManyToOne(targetEntity="AllForKids\MainBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $idBabysitter;

    /**
     * @var string
     *
     * @ORM\Column(name="monAvis", type="string", length=500)
     */
    private $monAvis;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idParent
     *
     * @param integer $idParent
     *
     * @return Avis
     */
    public function setIdParent($idParent)
    {
        $this->idParent = $idParent;

        return $this;
    }

    /**
     * Get idParent
     *
     * @return int
     */
    public function getIdParent()
    {
        return $this->idParent;
    }

    /**
     * Set idBabysitter
     *
     * @param integer $idBabysitter
     *
     * @return Avis
     */
    public function setIdBabysitter($idBabysitter)
    {
        $this->idBabysitter = $idBabysitter;

        return $this;
    }

    /**
     * Get idBabysitter
     *
     * @return int
     */
    public function getIdBabysitter()
    {
        return $this->idBabysitter;
    }

    /**
     * Set monAvis
     *
     * @param string $monAvis
     *
     * @return Avis
     */
    public function setMonAvis($monAvis)
    {
        $this->monAvis = $monAvis;

        return $this;
    }

    /**
     * Get monAvis
     *
     * @return string
     */
    public function getMonAvis()
    {
        return $this->monAvis;
    }
}

