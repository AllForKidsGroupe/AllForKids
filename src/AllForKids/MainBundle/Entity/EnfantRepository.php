<?php

namespace AllForKids\MainBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * EnfantRepository
 *
 * This class was generated by the PhpStorm "Php Annotations" Plugin. Add your own custom
 * repository methods below.
 */
class EnfantRepository extends EntityRepository
{
}
