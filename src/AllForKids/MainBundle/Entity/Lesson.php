<?php

namespace AllForKids\MainBundle\Entity;

use Doctrine\DBAL\Types\DateType;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
/**
 * Lesson
 *
 * @ORM\Table(name="lesson")
 * @ORM\Entity(repositoryClass="AllForKids\MainBundle\Repository\LessonRepository")
 */
class Lesson
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="brochure", type="string", length=255)
     * @Assert\NotBlank(message="Please, upload the product brochure as a PDF file.")
     * @Assert\File(mimeTypes={ "application/pdf" })
     */
    private $brochure;

    /**
     * @var string
     *
     * @ORM\Column(name="categorieAge", type="string", length=255)
     */
    private $categorieAge;

    /**
     * @var string
     *
     * @ORM\Column(name="theme", type="string", length=255)
     */
    private $theme;

    /**
     * @var DateType
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set brochure
     *
     * @param string $brochure
     *
     * @return Lesson
     */
    public function setBrochure($brochure)
    {
        $this->brochure = $brochure;

        return $this;
    }

    /**
     * Get brochure
     *
     * @return string
     */
    public function getBrochure()
    {
        return $this->brochure;
    }

    /**
     * Set categorieAge
     *
     * @param string $categorieAge
     *
     * @return Lesson
     */
    public function setCategorieAge($categorieAge)
    {
        $this->categorieAge = $categorieAge;

        return $this;
    }

    /**
     * Get categorieAge
     *
     * @return string
     */
    public function getCategorieAge()
    {
        return $this->categorieAge;
    }

    /**
     * Set theme
     *
     * @param string $theme
     *
     * @return Lesson
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @return DateType
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param DateType $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }


}

