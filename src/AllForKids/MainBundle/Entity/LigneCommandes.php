<?php

namespace AllForKids\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LigneCommandes
 *
 * @ORM\Table(name="ligne_commandes")
 * @ORM\Entity(repositoryClass="AllForKids\MainBundle\Repository\LigneCommandesRepository")
 */
class LigneCommandes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="PrixTotal", type="float")
     */
    private $prixTotal;

    /**
     * @return int
     */
    public function getNbrArticle()
    {
        return $this->nbrArticle;
    }

    /**
     * @param int $nbrArticle
     */
    public function setNbrArticle($nbrArticle)
    {
        $this->nbrArticle = $nbrArticle;
    }
    /**
     * @var integer
     *
     * @ORM\Column(name="nbrArticle", type="integer")
     */
    private $nbrArticle;

    /**
     * @return int
     */
    public function getIdProduit()
    {
        return $this->idProduit;
    }

    /**
     * @param int $idProduit
     */
    public function setIdProduit($idProduit)
    {
        $this->idProduit = $idProduit;
    }

    /**
     * @return int
     */
    public function getIdclient()
    {
        return $this->idclient;
    }

    /**
     * @param int $idclient
     */
    public function setIdclient($idclient)
    {
        $this->idclient = $idclient;
    }
    /**
     * @var int
     *
     * @ORM\Column(name="idProduit", type="integer")
     */
    private $idProduit;
    /**
     * @var int
     *
     * @ORM\Column(name="idclient", type="integer")
     */
    private $idclient;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prixTotal
     *
     * @param float $prixTotal
     *
     * @return LigneCommandes
     */
    public function setPrixTotal($prixTotal)
    {
        $this->prixTotal = $prixTotal;

        return $this;
    }

    /**
     * Get prixTotal
     *
     * @return float
     */
    public function getPrixTotal()
    {
        return $this->prixTotal;
    }

}

