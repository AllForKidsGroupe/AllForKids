<?php

namespace AllForKids\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Offre
 *
 * @ORM\Table(name="offre")
 * @ORM\Entity(repositoryClass="AllForKids\MainBundle\Repository\OffreRepository")
 */
class Offre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AllForKids\MainBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    public $parent;

    /**
     * @ORM\ManyToOne(targetEntity="AllForKids\MainBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    public $babysitter;

    /**
     * @var int
     *
     * @ORM\Column(name="NbrEnfant", type="integer")
     */
    private $nbrEnfant;

    /**
     * @var string
     *
     * @ORM\Column(name="Service", type="string", length=255)
     */
    private $service;

    /**
     * @var int
     *
     * @ORM\Column(name="Age", type="integer")
     */
    private $age;

    /**
     * @var \DateTime
     * @ORM\Column(name="DateOffre",type="datetime")
     * @Assert\GreaterThan("today")
     */
    private $DateOffre;

    /**
     * @var string
     *
     * @ORM\Column(name="temps", type="string", length=255)
     */
    private $temps;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Accept", type="boolean")
     */
    private $Accept=0;

    /**
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param string $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255)
     */
    private $ville;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nbrEnfant
     *
     * @param integer $nbrEnfant
     *
     * @return Offre
     */
    public function setNbrEnfant($nbrEnfant)
    {
        $this->nbrEnfant = $nbrEnfant;

        return $this;
    }

    /**
     * Get nbrEnfant
     *
     * @return int
     */
    public function getNbrEnfant()
    {
        return $this->nbrEnfant;
    }

    /**
     * Set service
     *
     * @param string $service
     *
     * @return Offre
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param int $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return string
     */
    public function getTemps()
    {
        return $this->temps;
    }

    /**
     * @param string $temps
     */
    public function setTemps($temps)
    {
        $this->temps = $temps;
    }

    /**
     * @return mixed
     */
    public function getBabysitter()
    {
        return $this->babysitter;
    }

    /**
     * @param mixed $babysitter
     */
    public function setBabysitter($babysitter)
    {
        $this->babysitter = $babysitter;
    }

    /**
     * @return bool
     */
    public function isAccept()
    {
        return $this->Accept;
    }

    /**
     * @param bool $Accept
     */
    public function setAccept($Accept)
    {
        $this->Accept = $Accept;
    }

    /**
     * @return mixed
     */
    public function getDateOffre()
    {
        return $this->DateOffre;
    }

    /**
     * @param mixed $DateOffre
     */
    public function setDateOffre($DateOffre)
    {
        $this->DateOffre = $DateOffre;
    }






}

