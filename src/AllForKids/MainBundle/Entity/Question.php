<?php

namespace AllForKids\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Question
 *
 * @ORM\Table(name="question")
 *  * @ORM\Entity(repositoryClass="AllForKids\MainBundle\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id;
    protected  $quiz;
    private $reponses;
    /**
     * @var string
     *
     * @ORM\Column(name="Libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\ManyToOne(targetEntity="Quiz")
     * @ORM\JoinColumn(referencedColumnName="id",onDelete="CASCADE")
     */
    private $idQuiz;

    /**
     * Question constructor.
     */
    public function __construct()
    {
        $this->reponses = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getReponses()
    {
        return $this->reponses;
    }

    /**
     * @param mixed $reponses
     */
    public function setReponses($reponses)
    {
        $this->reponses = $reponses;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Question
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set idQuiz
     *
     * @param integer $idQuiz
     *
     * @return Question
     */
    public function setIdQuiz($idQuiz)
    {
        $this->idQuiz = $idQuiz;


    }

    /**
     * Get idQuiz
     *
     * @return int
     */
    public function getIdQuiz()
    {
        return $this->idQuiz;
    }
    public function GetQuiz($quiz)
    {
        return $quiz;
    }
    public function setQuiz($quiz)
    {
        $this->quiz=$quiz;
        return $this;
    }
    public function addReponses(Reponse $reponse)
    {

        if (!$this->reponses->contains($reponse))
        {
            $reponse->setIdQuest($this);
            $this->reponses->add($reponse);

        }}

    public function removeTag(Reponse $reponse)
    {
        if ($this->reponses->contains($reponse)) {
            $this->reponses->removeElement($reponse);
        }
    }
}

