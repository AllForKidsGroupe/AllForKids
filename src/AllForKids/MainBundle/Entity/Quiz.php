<?php

namespace AllForKids\MainBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Quiz
 *
 * @ORM\Table(name="quiz")
 * @ORM\Entity(repositoryClass="AllForKids\MainBundle\Repository\QuizRepository")
 */
class Quiz
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_quiz", type="string", length=255)
     */
    private $nomQuiz;

    /**
     * @var string
     *
     * @ORM\Column(name="theme", type="string", length=255)
     */

    private $theme;

    /**
     * @var integer
     *
     * @ORM\Column(name="total", type="integer")
     */
    private $total;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_age", type="string", length=255)
     */



    private $categorieAge;
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;
    /**
     * @var int
     *
     * @ORM\Column(name="time", type="integer")
     */
    private $time;
    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @Assert\File(maxSize="2500k")
     */
    public $file;
    /**
     * @return int
     */
    public function getTime()
    {
        return $this->time;
    }

    protected  function getUploadDir(){

        return 'images';
    }



    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }


    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @param int $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    protected $questions;

    /**
     * Quiz constructor.
     */
    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param ArrayCollection $questions
     */
   public function setQuestions($questions)
    {
        $this->questions = $questions;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomQuiz
     *
     * @param string $nomQuiz
     *
     * @return Quiz
     */
    public function setNomQuiz($nomQuiz)
    {
        $this->nomQuiz = $nomQuiz;

        return $this;
    }

    /**
     * Get nomQuiz
     *
     * @return string
     */
    public function getNomQuiz()
    {
        return $this->nomQuiz;
    }

    /**
     * Set theme
     *
     * @param string $theme
     *
     * @return Quiz
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set categorieAge
     *
     * @param string $categorieAge
     *
     * @return Quiz
     */
    public function setCategorieAge($categorieAge)
    {
        $this->categorieAge = $categorieAge;

        return $this;
    }

    /**
     * Get categorieAge
     *
     * @return string
     */
    public function getCategorieAge()
    {
        return $this->categorieAge;
    }
    public function addQuestion(Question $question)
    {

        if (!$this->questions->contains($question))
        {
            $question->setIdQuiz($this);
            $this->questions->add($question);

    }}

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    public function removeQuestion(Question $question)
    {
        if ($this->questions->contains($question)) {
            $this->questions->removeElement($question);
        }
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }




}

