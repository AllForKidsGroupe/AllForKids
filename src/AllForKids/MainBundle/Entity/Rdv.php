<?php
/**
 * Created by PhpStorm.
 * User: IRONSIDE
 * Date: 2/27/2018
 * Time: 4:58 AM
 */
namespace AllForKids\MainBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\Table(name="rdv")
 */



class Rdv
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *@ORM\Column(type="datetime",nullable=true)
     */
    protected $date ;



    /**
 *@ORM\Column(type="string",nullable=true)
 */
    protected $nomEnfant ;

    /**
     *@ORM\ManyToOne(targetEntity="AllForKids\MainBundle\Entity\Vaccin", inversedBy="rdv")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $vaccin ;
    


    /**
     * @return mixed
     */
    public function getPediatre()
    {
        return $this->pediatre;
    }

    /**
     * @param mixed $pediatre
     */
    public function setPediatre($pediatre)
    {
        $this->pediatre = $pediatre;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     *@ORM\Column(type="string",nullable=true)
     */
    protected $pediatre ;

    /**
     * @return mixed
     */
    public function getNomEnfant()
    {
        return $this->nomEnfant;
    }

    /**
     * @param mixed $nomEnfant
     */
    public function setNomEnfant($nomEnfant)
    {
        $this->nomEnfant = $nomEnfant;
    }

    /**
     * @return mixed
     */
    public function getVaccin()
    {
        return $this->vaccin;
    }

    /**
     * @param mixed $vaccin
     */
    public function setVaccin($vaccin)
    {
        $this->vaccin = $vaccin;
    }




}