<?php
namespace AllForKids\MainBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var int
     *
     * @ORM\Column(name="Age", type="integer",nullable=true)
     */
    private $age;

    /**
     * @var string
     *
     * @ORM\Column(name="Sexe", type="string", length=255,nullable=true)
     */
    private $sexe;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255,nullable=true)
     */
    private $adresse;


    /**
     *  @ORM\Column(type="string", length=255,nullable=true)
     */
    private $nomImage;


    /**
     * @Assert\File(maxSize="2500k")
     */
    private $file;


    /**
     * @var string
     *
     * @ORM\Column(name="Ville", type="string", length=255,nullable=true)
     */
    private $ville;

    /**
     * @var int
     *
     * @ORM\Column(name="nbrAnneeExp", type="integer",nullable=true)
     */
    private $nbrAnneeExp;

    /**
     * @var string
     *
     * @ORM\Column(name="Etat", type="string", length=255,nullable=true)
     */
    private $etat;

    /**
     * @var int
     *
     * @ORM\Column(name="numTel", type="integer",nullable=true)
     */
    private $numTel;


    /**
     *@ORM\Column(type="string",nullable=true)
     */
    protected $telephone ;


    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="user_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;



    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="user_file", fileNameProperty="FileName")
     *
     * @var Files
     */
    private $Files;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $FileName;

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile( $image = null)
    {
        $this->imageFile = $image;

        if (null !== $image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImageName($imageName)
    {
        $this->imageName = $imageName;
    }

    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }






    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setFiles( $file = null)
    {
        $this->Files = $file;

    }

    public function getFiles()
    {
        return $this->Files;
    }

    public function setFileName($FileName)
    {
        $this->FileName = $FileName;
    }

    public function getFileName()
    {
        return $this->FileName;
    }






    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



    public function __construct()
    {
        parent::__construct();
        // your own logic
    }


    public function getWebPath()
    {
        return null=== $this->nomImage ? null :$this->getUploadDir.'/'.$this->nomImage;
    }
    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }
    protected  function getUploadDir(){

        return 'images';
    }

    public function UploadProfilePicture(){
        $this->file->move($this->getUploadRootDir(),$this->file->getClientOriginalName());
        $this->nomImage=$this->file->getClientOriginalName();
        $this->file=null;
    }

    /**
     * set nomImage
     *
     * @param string $nomImage
     *
     * @return Categorie
     */
    public function setNomImage($nomImage)
    {

        $this->nomImage==$nomImage;
        return $this;
    }

    /**
     * Get nomImage
     *
     * @return  string
     */


    public function getNomImage()
    {
        return $this->nomImage;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }



    /**
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }


    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;
    }

    /**
     * Set age
     *
     * @param integer $age
     *
     * @return ProfileBabysitter
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     *
     * @return ProfileBabysitter
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return ProfileBabysitter
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set nbrAnneeExp
     *
     * @param integer $nbrAnneeExp
     *
     * @return ProfileBabysitter
     */
    public function setNbrAnneeExp($nbrAnneeExp)
    {
        $this->nbrAnneeExp = $nbrAnneeExp;

        return $this;
    }

    /**
     * Get nbrAnneeExp
     *
     * @return int
     */
    public function getNbrAnneeExp()
    {
        return $this->nbrAnneeExp;
    }

    /**
     * Set etat
     *
     * @param string $etat
     *
     * @return ProfileBabysitter
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set numTel
     *
     * @param integer $numTel
     *
     * @return ProfileBabysitter
     */
    public function setNumTel($numTel)
    {
        $this->numTel = $numTel;

        return $this;
    }

    /**
     * Get numTel
     *
     * @return int
     */
    public function getNumTel()
    {
        return $this->numTel;
    }
}