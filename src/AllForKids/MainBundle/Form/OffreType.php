<?php

namespace AllForKids\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OffreType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nbrEnfant')
            ->add('service')
            ->add('age')
            ->add('DateOffre',DateType::class)
            ->add('temps',ChoiceType::class, array (
                'choices'=> array ('matin'=> "matin", 'aprés midi' =>"aprés midi",'soire'=>"soire",
                    'tout au long du jour'=>"tout au long du jour")))
            ->add('ville',ChoiceType::class,array('choices'=>array('Ariana'=>"Ariana",'Beja'=>"Beja",'Ben Arous'=>"Ben Arous"
            ,'Bizerte'=>"Bizerte",'Gabes'=>"Gabes",'Gafsa'=>"Gafsa",'Jendouba'=>"Jendouba",'Kairouan'=>"Kairouan",'Kasserine'=>"Kasserine",
                'Kebili'=>"Kebili",'Le Kef'=>"Le Kef","Mahdia"=>'Mahdia','La Manouba'=>"La Manouba","Medenine"=>'Medenine',
                'Monastir'=>"Monastir",'Nabeul'=>"Nabeul",'Sfax'=>"Sfax",'Sidi Bouzid'=>"Sidi Bouzid",'Siliana'=>"Siliana",'Sousse'=>"Sousse",
                'Tataouine'=>"Tataouine",'Tozeur'=>"Tozeur",'Tunis'=>"Tunis",'Zaghouan'=>"Zaghouan")))
            ->add('Ajout',SubmitType::class);
    }/**
 * {@inheritdoc}
 */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AllForKids\MainBundle\Entity\Offre'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'allforkids_mainbundle_offre';
    }


}
