<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 13/02/2018
 * Time: 15:48
 */

namespace AllForKids\MainBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Profile2Type extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Age')
            ->add('Ville',ChoiceType::class, array('choices'=>array('Ariana'=>"Ariana",'Beja'=>"Beja",'Ben Arous'=>"Ben Arous"
            ,'Bizerte'=>"Bizerte",'Gabes'=>"Gabes",'Gafsa'=>"Gafsa",'Jendouba'=>"Jendouba",'Kairouan'=>"Kairouan",'Kasserine'=>"Kasserine",
                'Kebili'=>"Kebili",'Le Kef'=>"Le Kef","Mahdia"=>'Mahdia','La Manouba'=>"La Manouba","Medenine"=>'Medenine',
                'Monastir'=>"Monastir",'Nabeul'=>"Nabeul",'Sfax'=>"Sfax",'Sidi Bouzid'=>"Sidi Bouzid",'Siliana'=>"Siliana",'Sousse'=>"Sousse",
            'Tataouine'=>"Tataouine",'Tozeur'=>"Tozeur",'Tunis'=>"Tunis",'Zaghouan'=>"Zaghouan")))
            ->add('Adresse')
            ->add('Sexe', ChoiceType::class, array('choices'=> array ('Femme'=> "Femme", 'Homme' =>"Homme")))
            ->add('nbrAnneeExp')
            ->add('Etat',ChoiceType::class,array('choices'=>array('Débutant'=>"Débutant",'Amateur'=>"Amateur",'Professionnel'=>"Professionnel")))
            ->add('numTel')
            ->add('file',FileType::class)
            ->add('Ajout',SubmitType::class);
    }/**
 * {@inheritdoc}
 */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AllForKids\MainBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'allforkids_mainbundle_Babysitter';
    }

}