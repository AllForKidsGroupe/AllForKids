<?php

namespace AllForKids\MainBundle\Form;

use AllForKids\MainBundle\Entity\Question;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class QuestionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('libelle');
        $builder->add('reponses', CollectionType::class, array(
            'entry_type' => \AllForKids\MainBundle\Form\ReponseType::class,
            'entry_options' => array('label' => false),
            'allow_add' => true,
            'by_reference' => false, 'allow_delete' => true,
            'prototype_name' => '__reponse__',
        ));


    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Question::class

        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'questions';
    }

    public function getName()
    {
        return 'questions';
    }
}
