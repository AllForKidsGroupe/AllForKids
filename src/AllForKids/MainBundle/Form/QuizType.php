<?php

namespace AllForKids\MainBundle\Form;

use AllForKids\MainBundle\Entity\Quiz;
use function Sodium\add;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AllForKids\MainBundle\Form\QuestionType;


class QuizType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nomQuiz')
            ->add('theme')
            ->add('categorieAge');
           $builder->add('questions', CollectionType::class, array(
               'entry_type' => \AllForKids\MainBundle\Form\QuestionType::class,
               'entry_options' => array('label' => false),
               'allow_add' => true,
               'by_reference' => false, 'allow_delete' => true,
           ))

           ->add('Rechercher',SubmitType::class)
        ;

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Quiz::class,
            'entry_options' => array('label' => false),
            'allow_add' => true,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'allforkids_mainbundle_quiz';
    }
    public function getName()
    {
        return 'quiz';
    }

}
