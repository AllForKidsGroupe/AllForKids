<?php

namespace AllForKids\MainBundle\Form;

use AllForKids\MainBundle\Entity\Vaccin;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RdvType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('date',DateTimeType::class,array(
                  'widget' => 'choice',
                    'minutes'=>array(00),
                    'hours'=>array(8,9,10,11,13,14,15,16),
                    'years'=> array(2018),
                    'model_timezone'=>	'Africa/Tunis' ,
           

                  ))
                 ->add('nomEnfant')
                 ->add('vaccin', EntityType::class, array(
                      // looks for choices from this entity
                       'class' => Vaccin::class,

                      // uses the User.username property as the visible option string
                       'choice_label' => 'nom',

                     // used to render a select box, check boxes or radios
                     // 'multiple' => true,
                     // 'expanded' => true,
                  ))
                 ->add('Ajouter',SubmitType::class);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AllForKids\MainBundle\Entity\Rdv'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'allforkids_mainbundle_rdv';
    }


}
