<?php

namespace AllForKids\MainBundle\Repository;

/**
 * LessonRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class LessonRepository extends \Doctrine\ORM\EntityRepository
{
    public function RechercheLesson($a,$b)
    {
        $query=$this->getEntityManager()
            ->createQuery("
select q from AllForKidsMainBundle:Lesson q where q.theme=:A and q.categorieAge=:B")
            ->setParameter('A',$a)
            ->setParameter('B',$b);
        return $query->getResult();
    }

}
