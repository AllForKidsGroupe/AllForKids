<?php

namespace AllForKids\MainBundle\Repository;

/**
 * OffreRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class OffreRepository extends \Doctrine\ORM\EntityRepository
{
    public function findville($ville)
    {
        $q=$this->createQueryBuilder('m')
            ->where('m.ville LIKE :ville')
            ->setParameter(':ville',"%$ville%");
        return $q->getQuery()->getResult();

    }
}
