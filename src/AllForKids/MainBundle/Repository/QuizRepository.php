<?php


namespace AllForKids\MainBundle\Repository;
/**
 * Created by PhpStorm.
 * User: khaoula.js
 * Date: 12/02/2018
 * Time: 07:40
 */
class QuizRepository  extends \Doctrine\ORM\EntityRepository
{
    public function RechercheQuiz($a,$b)
    {
        $query=$this->getEntityManager()
            ->createQuery("
select q from AllForKidsMainBundle:Quiz q where q.theme=:A and q.categorieAge=:B")
            ->setParameter('A',$a)
        ->setParameter('B',$b);
        return $query->getResult();
    }




}