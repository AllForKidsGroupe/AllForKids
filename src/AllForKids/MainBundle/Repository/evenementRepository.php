<?php

namespace AllForKids\MainBundle\Repository;

/**
 * evenementRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class evenementRepository extends \Doctrine\ORM\EntityRepository
{

    public function QB(){

        $query=$this->createQueryBuilder('a')
            ->where('a.categorie=:categorie')
            ->setParameter('categorie','2');

        return $query->getQuery()->getResult();

    }
    public function Cultiver(){

        $query=$this->createQueryBuilder('a')
            ->where('a.categorie=:categorie')
            ->setParameter('categorie','1');

        return $query->getQuery()->getResult();
    }
    public function Distraire(){

        $query=$this->createQueryBuilder('a')
            ->where('a.categorie=:categorie')
            ->setParameter('categorie','1');

        return $query->getQuery()->getResult();
    }


}
