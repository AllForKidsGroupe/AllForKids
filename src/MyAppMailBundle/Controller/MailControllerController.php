<?php

namespace MyAppMailBundle\Controller;
use Swift_Message;

use MyAppMailBundle\Entity\Mail;
use MyAppMailBundle\Form\MailType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MailControllerController extends Controller
{

    public function indexAction(Request $request)
    {
        $mail = new Mail();
        $form = $this->createForm(MailType::class, $mail);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $message = \Swift_Message::newInstance()
                ->setSubject('Accusé de réception')
                ->setFrom('espritplus2017@gmail.com')
                ->setTo($mail->getEmail())
                ->setBody(
                    $this->renderView(
                        '@MyAppMail/Mail/email.html.twig',
                        array('nom' => $mail->getNom(),'text' =>$mail->getText())

                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);
            return $this->redirect($this->generateUrl('my_app_mail_accuse'));
        }
        return $this->render('@MyAppMail/Mail/test.html.twig',
            array('form' => $form->createView()));
    }

      public function successAction(){
        return new Response("email envoyé avec succès, Merci de vérifier votre boite
        mail.");
      }


}
